import os, sys, json
import numpy as np
from datetime import datetime, date
from MonodomainFenics import ForwardEuler
import dolfin as fnx

parent_path = os.path.abspath(os.path.join(__file__ ,"../.."))
sys.path.append(parent_path)

from pipeline import _pipeline, _pipeline_without_saving

from multisimulation import get_possible_centers

#%% 

def _setup_simulation(center, identifier=None, task_num=None, **kwargs):
    start = datetime.now()
    current_time = start.strftime('%Y-%m-%d, %H:%M:%S')

    
    t = datetime.now().strftime("%y%m%d_%H%M%S")
    #simulation_name = t + "_" + str(_id) + "_" + str(rand_id)
    simulation_name = t + "_" + identifier
    
    info = {'simulation_name': simulation_name,
            'simulation_id':identifier,
            'stimulus_center':{'x':center[0], 'y':center[1], 'z':center[2]},
            'start_time': current_time,
            'task_num': task_num
            }
    
    if task_num != None:
        kwargs['task_num'] = task_num
    
    kwargs = {**kwargs, **info}
    
    return kwargs
    
#%%

def _simulate(identifier=None, without_saving=False, compute_on_cluster=False):
    with open(os.path.dirname(os.path.realpath(__file__)) + '/metadata.json', 'r') as f:
        kwargs = json.load(f)
    
    possible_centers = get_possible_centers(kwargs['base_folder'] + kwargs['path_to_mesh'])
    
    # choose n random coordinates from possible_centers
    if compute_on_cluster==False:
        if kwargs['mode'] == 'random':
            inds = np.random.randint(0,len(possible_centers),kwargs['num_simulations'])
        else:
            inds = range(0, kwargs['num_simulations'])
    
        centers = possible_centers[inds]
        
        for task_num, _center in enumerate(centers):
            _kwargs = _setup_simulation(_center, identifier=identifier, task_num=task_num, **kwargs)
            start = datetime.now()
            
             # this is the simulation!
            if without_saving:
                _pipeline_without_saving(**_kwargs)
            else:
                _pipeline(**_kwargs)
            
            end = datetime.now()
            current_time = end.strftime('%Y-%m-%d, %H:%M:%S')
            duration = (end-start).total_seconds()
            _kwargs['end_time'] = current_time
            _kwargs['duration (s)'] = duration
    
            simulation_info = _kwargs['metadata_output_folder'] + _kwargs['simulation_name'] + '.json'
            json.dump(_kwargs, open(simulation_info, 'w'), indent=8, sort_keys=False)
    

#%%
            
if __name__=='__main__':
    identifier = os.getenv('SGE_TASK_ID', 'TASK_ID') + '_' + os.getenv('SGE_JOB_ID', 'JOB_ID')
    _simulate(identifier = identifier, without_saving=1)
        
    
#%%






