import sys, os, json
import numpy as np
from datetime import datetime

#parent_path = os.path.abspath(os.path.join(__file__ ,"../.."))
#sys.path.append(parent_path)

#with open(os.path.dirname(os.path.realpath(__file__)) + '/metadata.json', 'r') as f:
#    kwargs = json.load(f)
    
from _MonodomainSimulation import monodomainSimulation
from _PhiHeartReconstruction import phiHeartReconstruction
from _DiffusionIntoBath import diffusionToBath
from _ElectrodeIntegration import electrodeIntegration


def _pipeline(only_init=False, **kwargs):
    name = kwargs['simulation_name']
    #rand_id = t + "_" + str(kwargs["simulation_id"]) + "_" + str(kwargs["rand_id"])
    
    print('monodomain')
    monodomain_sim = monodomainSimulation(output_folder = kwargs['monodomain_sim_output_dir'] + name, 
                                          only_init=only_init, 
                                          **kwargs)
    
    print('phi heart reconstruction')
    phi_heart_reconstruction = phiHeartReconstruction(input_folder = kwargs['monodomain_sim_output_dir'] + name,
                                                      output_folder = kwargs['phi_heart_reconstruction_output_dir'] + name, 
                                                      only_init=only_init,
                                                      **kwargs)
    
    print('diffusion to bath')
    diffuser_to_bath = diffusionToBath(input_folder = kwargs['phi_heart_reconstruction_output_dir'] + name,
                                       output_folder = kwargs['diffuser_to_bath_output_dir'] + name, 
                                       only_init=only_init,
                                       **kwargs)
    
    print('electrode integration')
    electrode_integrator = electrodeIntegration(input_folder = kwargs['diffuser_to_bath_output_dir'] + name,
                                                output_folder = kwargs['electrode_integrator_output_dir'], 
                                                only_init=only_init, 
                                                **kwargs)
    
    return monodomain_sim, phi_heart_reconstruction, diffuser_to_bath, electrode_integrator

def _pipeline_without_saving(**kwargs):
    print('Configure...')
    monodomain_sim, phi_heart_reconstruction, diffuser_to_bath, electrode_integrator = _pipeline(only_init=True, **kwargs)

    num_steps = (kwargs['stop'] - kwargs['start'])/ (kwargs['dt_fem']*kwargs['n_dt_skip'])

    ecgs = []
    for step in range(int(num_steps)):
        [monodomain_sim.one_step() for _ in range(kwargs['n_dt_skip'])]
        phi_heart_reconstruction.set_vm(monodomain_sim.u_n)
        phi_heart_reconstruction.reconstruct()
        diffuser_to_bath.set_phi(phi_heart_reconstruction.phi_n)
        diffuser_to_bath.diffuse()
        electrode_integrator.set_phi_all(diffuser_to_bath.phi_n_all)
        tags, vals = electrode_integrator.perform_integrations_for_current_time()
        ecgs.append(vals)
        
        name = f"{kwargs['electrode_integrator_output_dir']}ecgs_{kwargs['simulation_name']}"
        np.save(name, np.array(ecgs))
    
    #electrode_integrator._save(compact_save=True, _id=kwargs['simulation_name'])
        #print(f"Got signals {vals}")
        #print("_"*15)
        #print(num_steps)
    
    
    
    
    
    
    
    
    
    
    
    
    
    