from PotentialReconstruction import PhiHeartPotentialReconstructor
import dolfin as fnx

try:
    from parametertracking import Parametertracker
    parametertracker = Parametertracker()
except ImportError:
    print("Not using paametertracking class.")
    parametertracker = None

def phiHeartReconstruction(fenics_meshes_folder = None,
                           input_folder = None,
                           output_folder = None,
                           only_init=False,
                           heart_tag = 2126,
                           **kwargs):

    phi_heart_reconstruction = PhiHeartPotentialReconstructor(parametertracker)
    phi_heart_reconstruction.load_mesh(fenics_meshes_folder + "mesh.xdmf")
    phi_heart_reconstruction.load_markers(fenics_meshes_folder + "cellfunction.xdmf", fenics_meshes_folder + "facetfunction.xdmf")
    phi_heart_reconstruction.set_simulation_area(heart_tag)

    phi_heart_reconstruction.output_dir = output_folder
    phi_heart_reconstruction.load_ts_vm(input_folder + "/ts_u_n.h5")
    
    sigma_i = 2e-3 * fnx.Constant(((1, 0, 0), (0, 1, 0), (0, 0, 1))) # in S / cm
    lambda_factor = 0.75
    sigma_e = lambda_factor * sigma_i
    
    phi_heart_reconstruction.initialise_fem_formulation(sigma_e, sigma_i)
    phi_heart_reconstruction.setup_solver()
    
    if only_init==False:
        phi_heart_reconstruction.reconstruct_from_to_with_saving()
        return None
    
    else:
        return phi_heart_reconstruction