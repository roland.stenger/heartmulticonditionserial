from IntegrateOverSubdomain import RegionIntegrator

try:
    from parametertracking import Parametertracker
    parametertracker = Parametertracker()
except ImportError:
    print("Not using paametertracking class.")
    parametertracker = None

def electrodeIntegration(fenics_meshes_folder = None,
                         input_folder = None,
                         output_folder = None,
                         only_init=False,
                         heart_tag = 2126,
                         bath_tag = 2127,
                         simulation_name = 'xXx',
                         **kwargs):

    electrode_integrator = RegionIntegrator(parametertracker)
    electrode_integrator.load_mesh(fenics_meshes_folder + "mesh.xdmf")
    electrode_integrator.load_markers(fenics_meshes_folder + "cellfunction.xdmf", fenics_meshes_folder + "facetfunction.xdmf")
    electrode_integrator.output_dir = output_folder
    
    electrode_integrator.load_ts(input_folder + "/ts_phi_n_all_bath.h5")
    
    electrode_integrator.no_integration_tags = [0, heart_tag, bath_tag]
    # or
    electrode_tags = set(electrode_integrator.regions.array())
    [electrode_tags.discard(temp) for temp in [heart_tag, bath_tag, 0]]
    electrode_integrator.integration_tags = electrode_tags
    electrode_integrator.initialise_fem_formulation()
    
    #electrode_integrator.read_time()
    #tags, vals = electrode_integrator.perform_integrations_for_current_time()
    
    if only_init==False:
        electrode_integrator.integrate_from_to_with_storage(compact_save=True, _id=simulation_name)
        return None
    else:
        return electrode_integrator
