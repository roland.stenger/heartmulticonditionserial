from DiffusionInBath import DiffusionIntoBath
import dolfin as fnx

try:
    from parametertracking import Parametertracker
    parametertracker = Parametertracker()
except ImportError:
    print("Not using paametertracking class.")
    parametertracker = None

def diffusionToBath(fenics_meshes_folder = None,
                    input_folder = None,
                    output_folder = None,
                    only_init=False,
                    heart_tag = 2126,
                    bath_tag = 2127,
                    **kwargs):

    diffuser_to_bath = DiffusionIntoBath(parametertracker)
    diffuser_to_bath.load_mesh(fenics_meshes_folder + "mesh.xdmf")
    diffuser_to_bath.load_markers(fenics_meshes_folder + "cellfunction.xdmf", fenics_meshes_folder + "facetfunction.xdmf")
    diffuser_to_bath.heart_tag = heart_tag
    diffuser_to_bath.bath_tag = bath_tag
    diffuser_to_bath.interface_bath_heart_tag = 2128
    diffuser_to_bath.set_simulation_area("everywhere")
    
    # output dir
    
    diffuser_to_bath.output_dir = output_folder 
    diffuser_to_bath.load_ts_phi_heart(input_folder + "/ts_phi_n_whole_heart.h5")
    sigma_bath =  fnx.Constant(1) # in S / cm
    diffuser_to_bath.initialise_fem_formulation(sigma_bath)
    
    if only_init==False:
        diffuser_to_bath.diffuse_from_to_with_saving(step=kwargs['n_dt_skip'])
        return None
    else:
        return diffuser_to_bath
