from MonodomainFenics import ForwardEuler, IonicCurrents, AppliedCurrent 
# set up

try:
    from parametertracking import Parametertracker
    parametertracker = Parametertracker()
except ImportError:
    print("Not using paametertracking class.")
    parametertracker = None

def monodomainSimulation(fenics_meshes_folder = None,
                         output_folder = None,
                         stimulus_center = [0,0,0],
                         dt_fem = 0.1,
                         start=0.9, 
                         stop=10, 
                         saving_step_xdmf = [10,10],
                         only_init=False,
                         **kwargs):

    heart_tag, bath_tag = 2126, 2127
    
    sim = ForwardEuler(parametertracker=parametertracker) # if ps is None: don't track parameter, else: track them this can be down by self._params = lambda x: return x if ps is None, else ps und self.ps = ps
    
    sim.load_mesh(fenics_meshes_folder + "mesh.xdmf")
    sim.load_markers(fenics_meshes_folder + "cellfunction.xdmf", fenics_meshes_folder + "facetfunction.xdmf")
    sim.set_simulation_area(heart_tag)
    
    sim.output_dir = output_folder
    
    # set local current and pacing
    local_current = IonicCurrents.local_model("fh")
    sim.ionic_current = local_current
    
    stimulus_center_s = [stimulus_center['x'], stimulus_center['y'], stimulus_center['z']]
    # set pacing
    meshregion = AppliedCurrent.cylinder_region(sim.simulation_mesh, 
                                                stimulus_center_s, 
                                                r=1.5, 
                                                h=3)
    
    j_stim = AppliedCurrent.I_app(meshregion, 0.5)
    
    sigma = 1e-3

    sim.initialise_fem_formulation(sigma, 
                                   dt_fem, 
                                   j_stim=j_stim,
                                   element="CG", 
                                   degree=1)
    sim.setup_solver()
    
    if only_init==False:
        # simulate
        sim.run_with_saving(start, 
                            stop, 
                            save_xdmf=["u_n", "v"], 
                            saving_step_xdmf=saving_step_xdmf)
        return None
    
    else:
        return sim
    
    
