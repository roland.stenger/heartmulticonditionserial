from MonodomainFenics import ForwardEuler, IonicCurrents, AppliedCurrent 
from PotentialReconstruction import PhiHeartPotentialReconstructor
from DiffusionInBath import DiffusionIntoBath
from IntegrateOverSubdomain import RegionIntegrator
import dolfin as fnx

#%%
def get_possible_centers(path_to_mesh):
    """Returns the boundary coordinates of mesh m
    Usecase: Give simulation area, get bounary coordinates

    Parameters
    ----------
    m : dolfin mesh
        dolfin mesh of 

    Returns
    -------
    np.arrray with boundary mesh coordinates
    
    Example:
        possible_centers = get_boundary(monodomain_sim.simulation_mesh)
    """
    
    monodomain = ForwardEuler(parametertracker=None) 
    monodomain.load_mesh(path_to_mesh+"mesh.xdmf")
    monodomain.load_markers(path_to_mesh+"cellfunction.xdmf", path_to_mesh+"facetfunction.xdmf")
    monodomain.set_simulation_area(2126)
    
    b_mesh = fnx.BoundaryMesh(monodomain.simulation_mesh, 'exterior')
    
    return b_mesh.coordinates()


#%%
def simulate(title = "simulation",
             base_folder = "/home/roland/Projekte/Masterarbeit/simulations/",
             path_to_mesh = "2020-01-28_tests/FenicsMeshes/SimplifiedHeartInBath/",
             monodomain_sim_output_dir = "temp/",
             phi_heart_reconstruction_output_dir = "temp/",
             diffuser_to_bath_output_dir = "temp/",
             electrode_integrator_output_dir = "2020-01-28_tests/electrode_signals/",
             simulation_id = None,
             stimulus_center = [7.19403, 9.07232, 37.4364],
             start = 0,
             stop = 10,
             dt_fem = 0.1,
             **kwargs):
    
    """
    Parameters
    ----------
    path_to_mesh : path, string
        DESCRIPTION. The default is "/home/roland/Projekte/Masterarbeit/simulations/2020-01-28_tests/FenicsMeshes/SimplifiedHeartInBath/".
        The folder should contain mesh, facetfunction and cellfunction
        
    monodomain_sim_output_dir : path, string
        DESCRIPTION. The default is "/home/roland/Projekte/Masterarbeit/simulations/temp/".
        Stores {}.xdmf- (visualisation) and {}.h5-files (data-storage) files from solutions on each timestep

    Returns
    -------
    None.

    """
    
    #%%
    path_to_mesh = base_folder + path_to_mesh
    monodomain_sim_output_dir = base_folder + monodomain_sim_output_dir
    phi_heart_reconstruction_output_dir = base_folder + phi_heart_reconstruction_output_dir
    diffuser_to_bath_output_dir = base_folder + diffuser_to_bath_output_dir
    electrode_integrator_output_dir = base_folder + electrode_integrator_output_dir
    
    #%%
    heart_tag, bath_tag = 2126, 2127
    ######## monodomain calculation ########
    # set up
    try:
        from parametertracking import Parametertracker
        parametertracker = Parametertracker()
    except ImportError:
        print("Not using parametertracking class.")
        parametertracker = None
    
    monodomain_sim = ForwardEuler(parametertracker=parametertracker) 
    monodomain_sim.load_mesh(path_to_mesh+"mesh.xdmf")
    monodomain_sim.load_markers(path_to_mesh+"cellfunction.xdmf", path_to_mesh+"facetfunction.xdmf")
    monodomain_sim.set_simulation_area(heart_tag)
    monodomain_sim.output_dir = monodomain_sim_output_dir
    
    # set local current and pacing
    local_current = IonicCurrents.local_model("fh")
    monodomain_sim.ionic_current = local_current
    # set pacing
    #possible_centers = get_boundary(monodomain_sim.simulation_mesh)
    
    #stimulus_center = possible_centers[0] #[7.19403, 9.07232, 37.4364]
    meshregion = AppliedCurrent.cylinder_region(
            monodomain_sim.simulation_mesh, stimulus_center, r=1.5, h=3
    )
    j_stim = AppliedCurrent.I_app(meshregion, 0.5)
    
    # spatial discretization
    sigma = 1e-3
    dt_fem = dt_fem
    monodomain_sim.initialise_fem_formulation(sigma, dt_fem, j_stim=j_stim,
                                   element="CG", degree=1)
    monodomain_sim.setup_solver()
    # simulate
    #monodomain_sim.one_step()
    #monodomain_sim.run(start=1, stop=2)
    monodomain_sim.run_with_saving(
        start, stop, save_xdmf=["u_n", "v"], saving_step_xdmf=[100, 100],
    )
    
    #print('Simulation done')
    
    ######## potential reconstruction heart ########
    
    phi_heart_reconstruction = PhiHeartPotentialReconstructor(parametertracker)
    phi_heart_reconstruction.load_mesh(path_to_mesh + "mesh.xdmf")
    phi_heart_reconstruction.load_markers(path_to_mesh+"cellfunction.xdmf", path_to_mesh+"facetfunction.xdmf")
    phi_heart_reconstruction.set_simulation_area(heart_tag)
    # output dir
    input_dir = monodomain_sim.output_dir
    phi_heart_reconstruction.output_dir = phi_heart_reconstruction_output_dir
    phi_heart_reconstruction.load_ts_vm(input_dir + "/ts_u_n.h5")
    
    sigma_i = 2e-3 * fnx.Constant(((1, 0, 0), (0, 1, 0), (0, 0, 1))) # in S / cm
    lambda_factor = 0.75
    sigma_e = lambda_factor * sigma_i
    
    phi_heart_reconstruction.initialise_fem_formulation(sigma_e, sigma_i)
    phi_heart_reconstruction.setup_solver()
    phi_heart_reconstruction.reconstruct_from_to_with_saving()
    
    ######## diffusion into bath ########
    
    diffuser_to_bath = DiffusionIntoBath(parametertracker)
    diffuser_to_bath.load_mesh(path_to_mesh+"mesh.xdmf")
    diffuser_to_bath.load_markers(path_to_mesh+"cellfunction.xdmf", path_to_mesh+"facetfunction.xdmf")
    diffuser_to_bath.heart_tag = heart_tag
    diffuser_to_bath.bath_tag = bath_tag
    diffuser_to_bath.interface_bath_heart_tag = 2128
    diffuser_to_bath.set_simulation_area("everywhere")
    
    # output dir
    
    input_dir = phi_heart_reconstruction.output_dir
    diffuser_to_bath.output_dir = diffuser_to_bath_output_dir 
    diffuser_to_bath.load_ts_phi_heart(input_dir + "/ts_phi_n_whole_heart.h5")
    sigma_bath =  fnx.Constant(1) # in S / cm
    diffuser_to_bath.initialise_fem_formulation(sigma_bath)
    diffuser_to_bath.diffuse_from_to_with_saving(step=10)
    
    ### calculate electrode signals ###
    
    electrode_integrator = RegionIntegrator(parametertracker)
    electrode_integrator.load_mesh(path_to_mesh+"mesh.xdmf")
    electrode_integrator.load_markers(path_to_mesh+"cellfunction.xdmf", path_to_mesh+"facetfunction.xdmf")
    electrode_integrator.output_dir = electrode_integrator_output_dir
    
    input_dir = diffuser_to_bath_output_dir
    electrode_integrator.load_ts(input_dir + "/ts_phi_n_all_bath.h5")
    
    electrode_integrator.no_integration_tags = [0, heart_tag, bath_tag]
    # or
    electrode_tags = set(electrode_integrator.regions.array())
    [electrode_tags.discard(temp) for temp in [heart_tag, bath_tag, 0]]
    electrode_integrator.integration_tags = electrode_tags
    electrode_integrator.initialise_fem_formulation()
    
    electrode_integrator.read_time()
    tags, vals = electrode_integrator.perform_integrations_for_current_time()
    electrode_integrator.integrate_from_to_with_storage(compact_save=True, _id=simulation_id)

#%%

if __name__=='__main__':
    
    _kwargs = {"base_folder":"/home/roland/Projekte/Masterarbeit/simulations/",
             "path_to_mesh":"2020-01-28_tests/FenicsMeshes/SimplifiedHeartInBath/",
             "monodomain_sim_output_dir":"temp/",
             "phi_heart_reconstruction_output_dir":"temp/",
             "diffuser_to_bath_output_dir":"temp/",
             "electrode_integrator_output_dir":"2020-01-28_tests/electrode_signals/"}
    
    kwargs = {**_kwargs, "simulation_id":2}
    
    get_possible_centers(kwargs['base_folder'] + kwargs['path_to_mesh'])
    #simulate(**kwargs)
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    